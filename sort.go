// Sort defines interfaces and functions for sorting common types.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package sort

import (
	"sort"
)

// Lesser defines the Less method.
type Lesser interface {
	Less(i, j int) bool // Less reports whether the element with index i should sort before the element with index j.
}

// Swapper defines the Swap method.
type Swapper interface {
	Swap(i, j int) // Swap exchanges the i-th and j-th elements.
}

/////////////////////////////////////////////////////////////////////////
// parallelSort functions
/////////////////////////////////////////////////////////////////////////

// parallelSort implements the sort.Interface to allow parallel sorting.
type parallelSort struct {
	S sort.Interface // The objects to sort
	T []Swapper      // A slice of Swapper objects to rearrange in parallel
}

func (p *parallelSort) Len() int           { return p.S.Len() }
func (p *parallelSort) Less(i, j int) bool { return p.S.Less(i, j) }
func (p *parallelSort) Swap(i, j int) {
	p.S.Swap(i, j)
	for _, s := range p.T {
		s.Swap(i, j)
	}
}

/////////////////////////////////////////////////////////////////////////
// Convenience types for common cases
/////////////////////////////////////////////////////////////////////////

// IntSlice attaches the methods of Interface to []int, sorting in increasing order. Note that this is duplicated from the "sort" package for convenience.
type IntSlice []int

func (p IntSlice) Len() int           { return len(p) }
func (p IntSlice) Less(i, j int) bool { return p[i] < p[j] }
func (p IntSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p IntSlice) Sort() { sort.Sort(p) }

// Int8Slice attaches the methods of Interface to []int8, sorting in increasing order.
type Int8Slice []int8

func (p Int8Slice) Len() int           { return len(p) }
func (p Int8Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Int8Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Int8Slice) Sort() { sort.Sort(p) }

// Int16Slice attaches the methods of Interface to []int16, sorting in increasing order.
type Int16Slice []int16

func (p Int16Slice) Len() int           { return len(p) }
func (p Int16Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Int16Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Int16Slice) Sort() { sort.Sort(p) }

// Int32Slice attaches the methods of Interface to []int32, sorting in increasing order.
type Int32Slice []int32

func (p Int32Slice) Len() int           { return len(p) }
func (p Int32Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Int32Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Int32Slice) Sort() { sort.Sort(p) }

// Int64Slice attaches the methods of Interface to []int64, sorting in increasing order.
type Int64Slice []int64

func (p Int64Slice) Len() int           { return len(p) }
func (p Int64Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Int64Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Int64Slice) Sort() { sort.Sort(p) }

// UintSlice attaches the methods of Interface to []uint, sorting in increasing order.
type UintSlice []uint

func (p UintSlice) Len() int           { return len(p) }
func (p UintSlice) Less(i, j int) bool { return p[i] < p[j] }
func (p UintSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p UintSlice) Sort() { sort.Sort(p) }

// Uint8Slice attaches the methods of Interface to []uint8, sorting in increasing order.
type Uint8Slice []uint8

func (p Uint8Slice) Len() int           { return len(p) }
func (p Uint8Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Uint8Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Uint8Slice) Sort() { sort.Sort(p) }

// Uint16Slice attaches the methods of Interface to []uint16, sorting in increasing order.
type Uint16Slice []uint16

func (p Uint16Slice) Len() int           { return len(p) }
func (p Uint16Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Uint16Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Uint16Slice) Sort() { sort.Sort(p) }

// Uint32Slice attaches the methods of Interface to []uint32, sorting in increasing order.
type Uint32Slice []uint32

func (p Uint32Slice) Len() int           { return len(p) }
func (p Uint32Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Uint32Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Uint32Slice) Sort() { sort.Sort(p) }

// Uint64Slice attaches the methods of Interface to []uint64, sorting in increasing order.
type Uint64Slice []uint64

func (p Uint64Slice) Len() int           { return len(p) }
func (p Uint64Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Uint64Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Uint64Slice) Sort() { sort.Sort(p) }

// Float64Slice attaches the methods of Interface to []float64, sorting in increasing order.
type Float64Slice []float64

func (p Float64Slice) Len() int           { return len(p) }
func (p Float64Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Float64Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p Float64Slice) Sort() { sort.Sort(p) }

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SearchInts searches for x in a sorted slice of ints and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order. Note that this is duplicated from the "sort" package for convenience.
func SearchInts(a []int, x int) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchInt8s searches for x in a sorted slice of int8s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchInt8s(a []int8, x int8) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchInt16s searches for x in a sorted slice of int16s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchInt16s(a []int16, x int16) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchInt32s searches for x in a sorted slice of int32s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchInt32s(a []int32, x int32) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchInt64s searches for x in a sorted slice of int64s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchInt64s(a []int64, x int64) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchUints searches for x in a sorted slice of uints and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchUints(a []uint, x uint) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchUint8s searches for x in a sorted slice of uint8s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchUint8s(a []uint8, x uint8) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchUint16s searches for x in a sorted slice of uint16s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchUint16s(a []uint16, x uint16) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchUint32s searches for x in a sorted slice of uint32s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchUint32s(a []uint32, x uint32) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// SearchUint64s searches for x in a sorted slice of uint64s and returns the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
func SearchUint64s(a []uint64, x uint64) int {
	return sort.Search(len(a), func(i int) bool { return a[i] >= x })
}

// ParallelSort sorts the given arguments in parallel. S is placed in increasing order, whilst the entries in each of the T[i] are permuted accordingly. The T[i] must have length greater than or equal to the length of S, otherwise this may panic.
func ParallelSort(S sort.Interface, T ...Swapper) {
	sort.Sort(&parallelSort{S: S, T: T})
}

// ParallelStable stable sorts the given arguments in parallel. S is placed in increasing order, whilst the entries in each of the T[i] are permuted accordingly. The T[i] must have length greater than or equal to the length of S, otherwise this may panic.
func ParallelStable(S sort.Interface, T ...Swapper) {
	sort.Stable(&parallelSort{S: S, T: T})
}
